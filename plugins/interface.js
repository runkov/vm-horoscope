import Vue from 'vue';
import IWrapper from '~/components/I/IWrapper.vue';
import ICurtain from '~/components/I/ICurtain.vue';
import IButton from '~/components/I/IButton.vue';
import IStepper from '~/components/I/IStepper.vue';
import IInput from '~/components/I/IInput.vue';

Vue.component('IWrapper', IWrapper);
Vue.component('ICurtain', ICurtain);
Vue.component('IButton', IButton);
Vue.component('IStepper', IStepper);
Vue.component('IInput', IInput);
